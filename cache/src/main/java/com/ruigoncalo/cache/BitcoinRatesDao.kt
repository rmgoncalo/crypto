package com.ruigoncalo.cache

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.ruigoncalo.cache.model.BitcoinRatesCache
import io.reactivex.Single

@Dao
interface BitcoinRatesDao {

    @Query("SELECT * FROM bitcoinrates WHERE id = :id")
    fun getBitcoinRatesById(id: String): Single<BitcoinRatesCache>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBitcoinRates(bitcoinRates: BitcoinRatesCache)

    @Query("DELETE FROM bitcoinrates")
    fun deleteAllBitcoinRates()
}