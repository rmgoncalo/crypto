package com.ruigoncalo.cache.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "bitcoinratevalue")
data class BitcoinRateValueCache(
        @PrimaryKey
        val timestamp: Long,
        val value: Double)