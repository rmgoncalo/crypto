package com.ruigoncalo.cache.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "bitcoinrates")
data class BitcoinRatesCache(
        @PrimaryKey(autoGenerate = true)
        val id: Int,
        val name: String,
        val unit: String,
        val description: String,
        val values: List<BitcoinRatesCache>)