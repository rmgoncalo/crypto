package com.ruigoncalo.domain.utils

import java.text.NumberFormat
import java.util.*
import javax.inject.Inject

class CurrencyProvider @Inject constructor() {

    private val formatter by lazy {
        NumberFormat.getCurrencyInstance(Locale("EN"))
    }

    fun formatToCurrencyValue(code: String, value: Double): String {
        formatter.currency = convertToCurrency(code)
        return formatter.format(value)
    }

    private fun convertToCurrency(currencyCode: String): Currency {
        return Currency.getInstance(currencyCode)
    }
}