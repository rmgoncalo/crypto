package com.ruigoncalo.domain.utils

import java.text.DateFormat
import java.util.*
import javax.inject.Inject

class DateProvider @Inject constructor() {

    private val defaultLocale by lazy { Locale.getDefault() }

    private val dateFormatter = DateFormat.getDateInstance(DateFormat.DEFAULT, defaultLocale)

    fun formatToExtended(date: Date): String {
        return dateFormatter.format(date)
    }

    fun convertToDate(timestamp: Long): Date {
        return Date(timestamp * 1000)
    }
}