package com.ruigoncalo.domain

import io.reactivex.Observable

/**
 * Gets data using params and returns that data
 */

interface RetrieveInteractor<in Params, Result> {

    fun retrieve(params: Params): Observable<Result>
}