package com.ruigoncalo.domain

import io.reactivex.Completable
import io.reactivex.Observable
import polanski.option.Option

/**
 * The Repository drives data from the Data layer to the Domain layer (through interactors)
 * Defines the main methods we use to handle data (get, put, remove, etc)
 *
 *  getBitcoinRates: gets data from the store
 *  fetchBitcoinRates: fetch data from the remote source
 *
 */
interface Repository<Params, Value> {

    fun getBitcoinRates(params: Params): Observable<Option<Value>>

    fun fetchBitcoinRates(params: Params): Completable
}