package com.ruigoncalo.domain

import com.ruigoncalo.domain.model.BitcoinRates
import com.ruigoncalo.domain.model.Timespan
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import polanski.option.Option
import polanski.option.OptionUnsafe
import javax.inject.Inject

/**
 * Defines the main action: Get Bitcoin Rates
 *
 * Depends on the repository to get the data using get/fetch
 *
 * First approach should be to get the data and if fails fallback to fetch the data
 */
class GetBitcoinRatesInteractor @Inject constructor(
        private val repository: Repository<Timespan, BitcoinRates>) : RetrieveInteractor<Timespan, BitcoinRates> {

    override fun retrieve(params: Timespan): Observable<BitcoinRates> {
        return repository.getBitcoinRates(params)
                .flatMapSingle { fetchWhenNoneAndThenRetrieve(it, params) }
                .filter(Option<BitcoinRates>::isSome)
                .map { OptionUnsafe.getUnsafe(it) }
    }

    private fun fetchWhenNoneAndThenRetrieve(bitcoinRates: Option<BitcoinRates>,
                                             timespan: Timespan): Single<Option<BitcoinRates>> {
        return fetchWhenNone(bitcoinRates, timespan).andThen(Single.just(bitcoinRates))
    }

    private fun fetchWhenNone(bitcoinRates: Option<BitcoinRates>, timespan: Timespan): Completable {
        return if (bitcoinRates.isNone) repository.fetchBitcoinRates(timespan) else Completable.complete()
    }
}