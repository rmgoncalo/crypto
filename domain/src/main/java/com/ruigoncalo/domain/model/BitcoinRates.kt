package com.ruigoncalo.domain.model

data class BitcoinRates(val description: String,
                        val unit: String,
                        val values: List<BitcoinRateValue>)