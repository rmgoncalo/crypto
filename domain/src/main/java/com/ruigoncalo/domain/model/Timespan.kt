package com.ruigoncalo.domain.model

/**
 * Defines the timespan options to request rates
 */
enum class Timespan(val value: String) {
    DAYS30("30days"),
    DAYS60("60days"),
    DAYS180("180days"),
    YEAR1("1year"),
    YEAR2("2years"),
    ALL("all")
}