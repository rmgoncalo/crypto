package com.ruigoncalo.domain.model

import java.util.*

data class BitcoinRateValue(val timestamp: Date,
                            val value: Double)