package com.ruigoncalo.domain.utils

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import java.util.*
import kotlin.test.assertEquals

class DateProviderSpec : Spek({

    val tested = DateProvider()

    val mockTimestamp = 1527262200L
    val mockDate = Date(1527262200000)

    given("a timestamp") {

        it("should convert to date") {
            assertEquals(tested.convertToDate(mockTimestamp), mockDate)
        }
    }

    given("a date") {

        it("should transform date to extended format") {
            assertEquals(tested.formatToExtended(mockDate), "May 25, 2018")
        }
    }

})