package com.ruigoncalo.domain

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.reset
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.ruigoncalo.domain.model.BitcoinRates
import com.ruigoncalo.domain.model.Timespan
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import polanski.option.Option

class GetBitcoinRatesInteractorSpec : Spek({

    val repository: Repository<Timespan, BitcoinRates> = mock()
    val tested = GetBitcoinRatesInteractor(repository)
    var testObserver = TestObserver<BitcoinRates>()

    val mockBitcoinRates = BitcoinRates("desc", "USD", listOf())
    val mockTimespan = Timespan.DAYS180

    beforeEachTest {
        reset(repository)

        whenever(repository.fetchBitcoinRates(mockTimespan)).thenReturn(Completable.complete())

        testObserver = TestObserver()
    }

    describe("retrieve bitcoin rates") {

        context("no rates to get") {
            beforeEachTest {
                whenever(repository.getBitcoinRates(mockTimespan)).thenReturn(Observable.just(Option.none()))
                tested.retrieve(mockTimespan).subscribe(testObserver)
            }

            it("should fetch bitcoin rates") {
                verify(repository).fetchBitcoinRates(mockTimespan)
            }

            it("should return nothing and complete") {
                with(testObserver) {
                    assertNoValues()
                    assertNoErrors()
                    assertComplete()
                }
            }
        }

        context("there are rates") {
            beforeEachTest {
                whenever(repository.getBitcoinRates(mockTimespan)).thenReturn(Observable.just(Option.ofObj(mockBitcoinRates)))
                tested.retrieve(mockTimespan).subscribe(testObserver)
            }

            it("should not fetch bitcoin rates") {
                verify(repository, never()).fetchBitcoinRates(mockTimespan)
            }

            it("should return rates and complete") {
                with(testObserver) {
                    assertValue(mockBitcoinRates)
                    assertNoErrors()
                    assertComplete()
                }
            }
        }

        context("error getting rates") {
            val error = Throwable()
            beforeEachTest {
                whenever(repository.getBitcoinRates(mockTimespan)).thenReturn(Observable.error(error))
                tested.retrieve(mockTimespan).subscribe(testObserver)
            }

            it("should return error") {
                testObserver.assertError(error)
            }
        }

        context("error fetching rates") {
            val error = Throwable()
            beforeEachTest {
                whenever(repository.getBitcoinRates(mockTimespan)).thenReturn(Observable.just(Option.none()))
                whenever(repository.fetchBitcoinRates(mockTimespan)).thenReturn(Completable.error(error))
                tested.retrieve(mockTimespan).subscribe(testObserver)
            }

            it("should return error") {
                testObserver.assertError(error)
            }
        }
    }
})