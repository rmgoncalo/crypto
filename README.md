# Bitcoin Rates app

Get the average USD market price across major bitcoin exchanges for the last 30 days, 60 days, 180 days, year, 2 years or all time.

----
## Modules
The app's source code comprises three main modules:

* `app` (Android module)

* `domain` (Kotlin module)

* `data` (Kotlin moduleª)

ª - For simplicity, in this app the persistence layer is just memory, but in case it was a database or files (ex: `SharedPreferences`) the `cache` directory within the `data` module should be a separate module itself (dependent on Android libs).

## `domain` module

Defines the app's main actions and object entities. This app have only one main action: *retrieve bitcoin rates*. Therefore, an interactor was created to execute this action: `RetrieveInteractor`. It declares a `retrieve(params: Params): Observable<Result>` method that will provide `Result`s according to the given `Params`. The `GetBitcoinRatesInteractor` implements the `RetrieveInteractor` with the appropriate logic to provide the bitcoin rates.

The main object entities are `BitcoinRates` that represent a list of bitcoin rates values over time, each instant being defined by a `BitcoinRateValue`. There's also a `Timespan` that specifies the time frame in which the bitcoin rate values should hold.

At last, there's a `Repository` interface that bridges the `domain` and `data` modules. This interface establishes a contract that the `data` module must implement.

## `data` module

Aims at fetching and storing data both remotely and locally.  The `/remote` directory holds all classes that take care of fetching data remotely. Similarly, the `/cache` directory holds all classes that allow getting and storing data locally.

This module provides a `ReactiveStore` that is built on top of the cache layer to establish a *reactive* behavior when objects are stored locally.

As mention above, the `Repository` is the only interface the `data` module has to exchange data. Therefore, a `BitcoinRatesRepository` manages to work with both the remote and store objects. The `getBitcoinRates(params: Timespan): Observable<Option<BitcoinRates>>` method tries to get data from the store. The `fetchBitcoinRates(params: Timespan): Completable` method gets data remotely and stores it locally.

## `app` module

Contains a `/presentation` directory that comprises the view entities and view model classes that define the user interface, following the *MVVM pattern*. Since the app displays only one screen - `CryptoActivity` - there's only one view model associated -`BitcoinRatesViewModel`. This view model depends on the `GetBitcoinRatesInteractor` to get the desired data. So, it calls the interactor to get the data and posts it through a `LiveData` object. In case something goes wrong, an error message is also posted.

At last, the `CryptoActivity` observes the view model and renders the view according to its values. It also provides buttons to select the `Timespan`. The view model receives this request and replies with the new view state - represented by `Resource` objects.

## Build and run

1. Create a `local.properties` with `sdk.dir=<path_to_android_sdk>`and add to the root of the project

2. Run `./gradlew app:assemble` to generate the apk

### Tests

All modules have unit tests (using the [Spek framework](https://spekframework.org/))

Run `./gradlew test` to run tests from all modules

Run `./gradlew <module>:test` to run test from a specific module

Run `./gradlew <module>:test --tests com.ruigoncalo...<ClassNameSpec.kt>` to run a specific test file

## Known issues

In some Android devices the `NumberFormat` formatter may throw Exception or not generate properly the currency format.