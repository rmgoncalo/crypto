package com.ruigoncalo.crypto.presentation.model

data class BitcoinRateValueViewEntity(val entryX: Float,
                                      val entryY: Float,
                                      val entryData: BitcoinRateEntryViewEntity)

data class BitcoinRateEntryViewEntity(val rate: String,
                                      val date: String,
                                      val message: String)