package com.ruigoncalo.crypto.presentation

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.ruigoncalo.crypto.presentation.model.BitcoinRatesViewEntity
import com.ruigoncalo.crypto.presentation.state.Resource
import com.ruigoncalo.crypto.utils.StringsProvider
import com.ruigoncalo.data.remote.NetworkException
import com.ruigoncalo.domain.GetBitcoinRatesInteractor
import com.ruigoncalo.domain.model.Timespan
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class BitcoinRatesViewModel @Inject constructor(
        private val getBitcoinRatesInteractor: GetBitcoinRatesInteractor,
        private val mapper: BitcoinRatesViewEntityMapper,
        private val stringsProvider: StringsProvider) : ViewModel() {

    private val disposables = CompositeDisposable()

    private val bitcoinRatesData: MutableLiveData<Resource<BitcoinRatesViewEntity>> = MutableLiveData()

    fun getBitcoinRatesData(): MutableLiveData<Resource<BitcoinRatesViewEntity>> {
        return bitcoinRatesData
    }

    fun getBitcoinRates(timespan: Timespan = Timespan.DAYS180) {
        bitcoinRatesData.postValue(Resource.loading())
        disposables.add(
                getBitcoinRatesInteractor.retrieve(timespan)
                        .observeOn(Schedulers.computation())
                        .map { mapper.map(it) }
                        .subscribe({
                            bitcoinRatesData.postValue(Resource.success(it))
                        }, { throwable ->
                            if(throwable is NetworkException) {
                                when (throwable .kind) {
                                    NetworkException.Kind.NETWORK -> {
                                        bitcoinRatesData.postValue(Resource.error(stringsProvider.networkError()))
                                    }

                                    else -> {
                                        bitcoinRatesData.postValue(Resource.error(stringsProvider.unknownError()))
                                    }
                                }
                            } else {
                                bitcoinRatesData.postValue(Resource.error(stringsProvider.unknownError()))
                            }
                        }))
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }

}