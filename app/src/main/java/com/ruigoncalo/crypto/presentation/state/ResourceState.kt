package com.ruigoncalo.crypto.presentation.state

/**
 * Source:
 * https://github.com/hitherejoe/GithubTrending
 */
enum class ResourceState {
    LOADING, SUCCESS, ERROR
}