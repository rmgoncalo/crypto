package com.ruigoncalo.crypto.presentation

import com.ruigoncalo.crypto.presentation.model.BitcoinRateEntryViewEntity
import com.ruigoncalo.crypto.presentation.model.BitcoinRateValueViewEntity
import com.ruigoncalo.crypto.presentation.model.BitcoinRatesViewEntity
import com.ruigoncalo.domain.Mapper
import com.ruigoncalo.domain.model.BitcoinRates
import com.ruigoncalo.domain.utils.CurrencyProvider
import com.ruigoncalo.domain.utils.DateProvider
import javax.inject.Inject

class BitcoinRatesViewEntityMapper @Inject constructor(
        private val dateProvider: DateProvider,
        private val currencyProvider: CurrencyProvider) : Mapper<BitcoinRates, BitcoinRatesViewEntity> {

    override fun map(r: BitcoinRates): BitcoinRatesViewEntity {
        return BitcoinRatesViewEntity(
                r.description,
                r.values.mapIndexed { index, bitcoinRateValue ->
                    BitcoinRateValueViewEntity(
                            index.toFloat(),
                            bitcoinRateValue.value.toFloat(),
                            BitcoinRateEntryViewEntity(
                                    currencyProvider.formatToCurrencyValue(r.unit, bitcoinRateValue.value),
                                    dateProvider.formatToExtended(bitcoinRateValue.timestamp),
                                    generateMessage(bitcoinRateValue.value))
                    )
                })
    }

    private fun generateMessage(value: Double): String {
        return if (value > 10000) "SELL" else "HODL"
    }
}