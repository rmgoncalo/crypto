package com.ruigoncalo.crypto.presentation.model

data class BitcoinRatesViewEntity(val description: String,
                                  val values: List<BitcoinRateValueViewEntity>)