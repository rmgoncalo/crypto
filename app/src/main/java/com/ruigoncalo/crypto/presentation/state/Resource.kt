package com.ruigoncalo.crypto.presentation.state

import polanski.option.Option

/**
 * Source:
 * https://github.com/hitherejoe/GithubTrending
 */
class Resource<T> constructor(val state: ResourceState,
                              val data: Option<T>,
                              val message: Option<String>) {

    companion object {

        fun <T> success(data: T): Resource<T> {
            return Resource(ResourceState.SUCCESS, Option.ofObj(data), Option.none())
        }

        fun <T> error(message: String?): Resource<T> {
            return Resource(ResourceState.ERROR, Option.none(), Option.ofObj(message))
        }

        fun <T> loading(): Resource<T> {
            return Resource(ResourceState.LOADING, Option.none(), Option.none())
        }
    }
}