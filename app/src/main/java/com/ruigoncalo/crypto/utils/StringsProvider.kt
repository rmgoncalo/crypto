package com.ruigoncalo.crypto.utils


interface StringsProvider {

    fun networkError(): String

    fun unknownError(): String
}