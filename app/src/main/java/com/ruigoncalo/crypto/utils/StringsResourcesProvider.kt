package com.ruigoncalo.crypto.utils

import android.content.Context
import com.ruigoncalo.crypto.R
import javax.inject.Inject

class StringsResourcesProvider @Inject constructor(private val context: Context) : StringsProvider {

    override fun networkError(): String {
        return context.getString(R.string.network_error)
    }

    override fun unknownError(): String {
        return context.getString(R.string.unknown_error)
    }
}