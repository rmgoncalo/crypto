package com.ruigoncalo.crypto.injection

import android.util.Log
import com.ruigoncalo.data.remote.BlockchainApi
import com.ruigoncalo.data.remote.RetrofitFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import javax.inject.Singleton



@Module
class RemoteModule {

    @Provides
    @Singleton
    fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor { message -> Log.d("OkHttp", message) }
                .setLevel(HttpLoggingInterceptor.Level.BASIC)
    }

    @Provides
    fun provideClient(interceptor: HttpLoggingInterceptor) : OkHttpClient {
        return OkHttpClient.Builder().addInterceptor(interceptor).build()
    }

    @Provides
    fun provideRetrofitFactory() = RetrofitFactory()

    @Provides
    @Singleton
    fun provideBlockchainApi(retrofitFactory: RetrofitFactory, client: OkHttpClient): BlockchainApi {
        return retrofitFactory
                .build(client)
                .create(BlockchainApi::class.java)
    }
}