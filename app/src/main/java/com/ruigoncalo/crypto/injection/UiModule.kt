package com.ruigoncalo.crypto.injection

import com.ruigoncalo.crypto.ui.CryptoActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class UiModule {

    @ContributesAndroidInjector
    abstract fun contributesCryptoActivity(): CryptoActivity

}