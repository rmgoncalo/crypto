package com.ruigoncalo.crypto.injection

import com.ruigoncalo.data.BitcoinRatesMapper
import com.ruigoncalo.data.BitcoinRatesRepository
import com.ruigoncalo.data.cache.Cache
import com.ruigoncalo.data.cache.MemoryCache
import com.ruigoncalo.data.remote.BlockchainApi
import com.ruigoncalo.data.store.ReactiveStore
import com.ruigoncalo.data.store.Store
import com.ruigoncalo.domain.Repository
import com.ruigoncalo.domain.model.BitcoinRates
import com.ruigoncalo.domain.model.Timespan
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataModule {

    @Provides
    @Singleton
    fun providesCache(): Cache<String, BitcoinRates> {
        return MemoryCache()
    }

    @Provides
    fun providesStore(cache: Cache<String, BitcoinRates>): Store<String, BitcoinRates> {
        return ReactiveStore(cache)
    }

    @Provides
    fun providesRepository(api: BlockchainApi, store: Store<String, BitcoinRates>, mapper: BitcoinRatesMapper): Repository<Timespan, BitcoinRates> {
        return BitcoinRatesRepository(api, store, mapper)
    }
}