package com.ruigoncalo.crypto.injection

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.ruigoncalo.crypto.presentation.BitcoinRatesViewModel
import com.ruigoncalo.crypto.utils.StringsProvider
import com.ruigoncalo.crypto.utils.StringsResourcesProvider
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import kotlin.reflect.KClass

@Module
abstract class PresentationModule {

    @Binds
    @IntoMap
    @ViewModelKey(BitcoinRatesViewModel::class)
    abstract fun bindBitcoinRatesViewModel(viewModel: BitcoinRatesViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    abstract fun bindStringProvider(provider: StringsResourcesProvider): StringsProvider
}

@MustBeDocumented
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
@MapKey
annotation class ViewModelKey(val value: KClass<out ViewModel>)