package com.ruigoncalo.crypto.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.res.Configuration.ORIENTATION_LANDSCAPE
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.ruigoncalo.crypto.R
import com.ruigoncalo.crypto.injection.ViewModelFactory
import com.ruigoncalo.crypto.presentation.BitcoinRatesViewModel
import com.ruigoncalo.crypto.presentation.model.BitcoinRateEntryViewEntity
import com.ruigoncalo.crypto.presentation.model.BitcoinRatesViewEntity
import com.ruigoncalo.crypto.presentation.state.Resource
import com.ruigoncalo.crypto.presentation.state.ResourceState
import com.ruigoncalo.crypto.utils.color
import com.ruigoncalo.crypto.utils.onClick
import com.ruigoncalo.crypto.utils.toGone
import com.ruigoncalo.crypto.utils.toInvisible
import com.ruigoncalo.crypto.utils.toVisible
import com.ruigoncalo.domain.model.Timespan
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_crypto.*
import polanski.option.OptionUnsafe
import javax.inject.Inject

class CryptoActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: BitcoinRatesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_crypto)
        AndroidInjection.inject(this)

        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(BitcoinRatesViewModel::class.java)

        setupViews()
        hideLoading()
        hideRateValueContainer()
        hideError()

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.getBitcoinRatesData().observe(this,
                Observer<Resource<BitcoinRatesViewEntity>> { resource ->
                    resource?.let {
                        when (it.state) {
                            ResourceState.SUCCESS -> {
                                if (it.data.isSome) {
                                    val viewEntity = OptionUnsafe.getUnsafe(it.data)
                                    showDescription(viewEntity.description)
                                    showChart(viewEntity)
                                    showRateValueContainer()
                                    hideLoading()
                                    hideError()
                                }
                            }

                            ResourceState.ERROR -> {
                                if (it.message.isSome) {
                                    showError(OptionUnsafe.getUnsafe(it.message))
                                    hideLoading()
                                    hideRateValueContainer()
                                }
                            }

                            ResourceState.LOADING -> {
                                showLoading()
                                hideRateValueContainer()
                                hideError()
                            }
                        }
                    }
                })

        viewModel.getBitcoinRates()
    }

    private fun setupViews() {
        button30days.onClick { viewModel.getBitcoinRates(Timespan.DAYS30) }
        button60days.onClick { viewModel.getBitcoinRates(Timespan.DAYS60) }
        button180days.onClick { viewModel.getBitcoinRates(Timespan.DAYS180) }
        button1year.onClick { viewModel.getBitcoinRates(Timespan.YEAR1) }
        button2years.onClick { viewModel.getBitcoinRates(Timespan.YEAR2) }
        buttonAll.onClick { viewModel.getBitcoinRates(Timespan.ALL) }
    }

    private fun showDescription(description: String) {
        descriptionLabel.text = description
    }

    private fun showChart(chartPoints: BitcoinRatesViewEntity) {
        val entries = chartPoints.values.map { item -> Entry(item.entryX, item.entryY, item.entryData) }
        val dataSet = buildDataSet(entries)

        buildHorizontalAxis(chartView)
        buildVerticalAxis(chartView)

        with(chartView) {
            setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
                override fun onValueSelected(entry: Entry, highlight: Highlight) {
                    val entryData = entry.data as BitcoinRateEntryViewEntity
                    rateLabel.text = entryData.rate
                    dateLabel.text = entryData.date
                    messageLabel.text = entryData.message
                }

                override fun onNothingSelected() {
                    // empty
                }
            })

            setDrawGridBackground(false)
            setTouchEnabled(true)
            setPinchZoom(true)
            description.isEnabled = false
            legend.isEnabled = false
            isDragEnabled = true
            setScaleEnabled(true)
            setViewPortOffsets(0f, 0f, 0f, 0f)

            data = LineData(dataSet)
            val midPoint = chartPoints.values.size / 2
            highlightValue(midPoint.toFloat(), 0, true)
            animateX(500, Easing.EasingOption.Linear)
            invalidate()
        }
    }

    private fun buildVerticalAxis(chart: LineChart) {
        with(chart) {
            axisRight.isEnabled = false
            axisLeft.apply {
                textColor = color(R.color.colorPrimaryDark)
                axisLineColor = color(R.color.colorPrimary)
                setLabelCount(if (isLandscapeOrientation()) 5 else 8, false)
                setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART)
                setDrawGridLines(false)
            }
        }
    }

    private fun buildHorizontalAxis(chart: LineChart) {
        with(chart.xAxis) {
            setDrawLabels(false)
            setDrawGridLines(false)
        }
    }

    private fun buildDataSet(values: List<Entry>): LineDataSet {
        return LineDataSet(values, null).apply {
            fillColor = color(R.color.white)
            highLightColor = color(R.color.colorPrimaryDark)
            color = color(R.color.colorPrimary)

            setDrawFilled(true)
            setDrawCircles(false)
            mode = LineDataSet.Mode.LINEAR

            lineWidth = 2.5f
            highlightLineWidth = 1.8f
        }
    }

    private fun showRateValueContainer() {
        rateValueContainer.toVisible()
    }

    private fun hideRateValueContainer() {
        rateValueContainer.toInvisible()
    }

    private fun showLoading() {
        loadingView.toVisible()
    }

    private fun hideLoading() {
        loadingView.toGone()
    }

    private fun showError(message: String) {
        errorView.text = message
        errorView.toVisible()
    }

    private fun hideError() {
        errorView.toGone()
    }

    private fun isLandscapeOrientation() =
            resources.configuration.orientation == ORIENTATION_LANDSCAPE
}
