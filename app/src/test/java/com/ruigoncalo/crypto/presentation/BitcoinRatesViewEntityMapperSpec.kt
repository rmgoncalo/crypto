package com.ruigoncalo.crypto.presentation

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.reset
import com.nhaarman.mockito_kotlin.whenever
import com.ruigoncalo.domain.model.BitcoinRateValue
import com.ruigoncalo.domain.model.BitcoinRates
import com.ruigoncalo.domain.utils.CurrencyProvider
import com.ruigoncalo.domain.utils.DateProvider
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import java.util.*
import kotlin.test.assertEquals

class BitcoinRatesViewEntityMapperSpec : Spek({

    val dateProvider: DateProvider = mock()
    val currencyProvider: CurrencyProvider = mock()
    val tested = BitcoinRatesViewEntityMapper(dateProvider, currencyProvider)

    val date = Date()
    val dateExtended = "extended"

    val currency = "USD"
    val currencyFormatted = "currency-value"

    beforeEachTest {
        whenever(dateProvider.formatToExtended(date)).thenReturn(dateExtended)
        whenever(currencyProvider.formatToCurrencyValue(currency, 1000.0)).thenReturn(currencyFormatted)
    }

    afterEachTest {
        reset(dateProvider)
        reset(currencyProvider)
    }

    given("bitcoin rates mapper and a bitcoin rate object") {
        val mockBitcoinRates = BitcoinRates("desc", currency,
                listOf(BitcoinRateValue(date, 1000.0)))

        it("should map the same description") {
            System.out.println("Desc=${tested.map(mockBitcoinRates).description}")
            assertEquals(tested.map(mockBitcoinRates).description, mockBitcoinRates.description)
        }

        it("should map the values using index and rate value") {
            assertEquals(tested.map(mockBitcoinRates).values.first().entryX, 0f)
            assertEquals(tested.map(mockBitcoinRates).values.first().entryY, 1000f)
        }

        it("should create a BitcoinRateEntryViewEntity") {
            assertEquals(tested.map(mockBitcoinRates).values.first().entryData.rate, currencyFormatted)
            assertEquals(tested.map(mockBitcoinRates).values.first().entryData.date, dateExtended)
        }

        given("rate value is <= 10000") {

            it("should add message HODL") {
                assertEquals(tested.map(mockBitcoinRates).values.first().entryData.message, "HODL")
            }
        }

        given("rate value is > 10000") {
            val mockBitcoinBigRate = BitcoinRates("desc", currency,
                    listOf(BitcoinRateValue(date, 15000.0)))

            beforeEachTest {
                whenever(currencyProvider.formatToCurrencyValue(currency, 15000.0)).thenReturn(currencyFormatted)
            }

            it("should add message SELL") {
                assertEquals(tested.map(mockBitcoinBigRate).values.first().entryData.message, "SELL")
            }
        }
    }
})