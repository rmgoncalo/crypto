package com.ruigoncalo.crypto.presentation

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.reset
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.ruigoncalo.crypto.TestLiveDataObserver
import com.ruigoncalo.crypto.presentation.model.BitcoinRatesViewEntity
import com.ruigoncalo.crypto.presentation.state.Resource
import com.ruigoncalo.crypto.rxGroup
import com.ruigoncalo.crypto.testObserver
import com.ruigoncalo.crypto.utils.StringsProvider
import com.ruigoncalo.data.remote.NetworkException
import com.ruigoncalo.domain.GetBitcoinRatesInteractor
import com.ruigoncalo.domain.model.BitcoinRates
import com.ruigoncalo.domain.model.Timespan
import io.reactivex.Observable
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import java.io.IOException
import kotlin.test.assertEquals

class BitcoinRatesViewModelSpec : Spek({

    rxGroup("bitcoin rates view model") {

        val getBitcoinRatesInteractor: GetBitcoinRatesInteractor = mock()
        val mapper: BitcoinRatesViewEntityMapper = mock()
        val stringsProvider: StringsProvider = mock()
        var tested: BitcoinRatesViewModel? = null
        var liveData: TestLiveDataObserver<Resource<BitcoinRatesViewEntity>>? = null


        val mockTimespan = Timespan.DAYS180
        val mockBitcoinRates = BitcoinRates("desc", "USD", listOf())
        val mockBitcoinRatesViewEntity = BitcoinRatesViewEntity("desc", listOf())

        val stringNetwork = "network"
        val stringsUnknown = "unknown"

        val successResource = Resource.success(mockBitcoinRatesViewEntity)
        val networkErrorResource = Resource.error<BitcoinRatesViewEntity>(stringNetwork)
        val unknownErrorResource = Resource.error<BitcoinRatesViewEntity>(stringsUnknown)
        val loadingResource = Resource.loading<BitcoinRatesViewEntity>()

        val networkException = NetworkException.networkError(IOException(("")))
        val unexpectedError = NetworkException.unexpectedError(IOException(("")))

        beforeEachTest {
            whenever(mapper.map(mockBitcoinRates)).thenReturn(mockBitcoinRatesViewEntity)
            whenever(stringsProvider.networkError()).thenReturn(stringNetwork)
            whenever(stringsProvider.unknownError()).thenReturn(stringsUnknown)

            tested = BitcoinRatesViewModel(getBitcoinRatesInteractor, mapper, stringsProvider)
            liveData = tested!!.getBitcoinRatesData().testObserver()
        }

        afterEachTest {
            reset(getBitcoinRatesInteractor)
            reset(mapper)
            reset(stringsProvider)
        }

        describe("get bitcoin rates success") {
            beforeEachTest {
                whenever(getBitcoinRatesInteractor.retrieve(mockTimespan)).thenReturn(Observable.just(mockBitcoinRates))
                tested?.getBitcoinRates()
            }

            it("should show loading") {
                assertEquals(liveData!!.observedValues[0]!!.state, loadingResource.state)
            }

            it("should run interactor") {
                verify(getBitcoinRatesInteractor).retrieve(mockTimespan)
            }

            it("should map to view entity") {
                verify(mapper).map(mockBitcoinRates)
            }

            it("should post values") {
                assertEquals(liveData!!.observedValues[1]!!.state, successResource.state)
            }
        }

        describe("get bitcoin rates network error") {
            beforeEachTest {
                whenever(getBitcoinRatesInteractor.retrieve(mockTimespan)).thenReturn(Observable.error(networkException))
                tested?.getBitcoinRates()
            }

            it("should show loading") {
                assertEquals(liveData!!.observedValues[0]!!.state, loadingResource.state)
            }

            it("should not map to view entity") {
                verify(mapper, never()).map(mockBitcoinRates)
            }

            it("should show network error") {
                assertEquals(liveData!!.observedValues[1]!!.state, networkErrorResource.state)
            }
        }

        describe("get bitcoin rates unknown error") {
            beforeEachTest {
                whenever(getBitcoinRatesInteractor.retrieve(mockTimespan)).thenReturn(Observable.error(unexpectedError))
                tested?.getBitcoinRates()
            }

            it("should show loading") {
                assertEquals(liveData!!.observedValues[0]!!.state, loadingResource.state)
            }

            it("should not map to view entity") {
                verify(mapper, never()).map(mockBitcoinRates)
            }

            it("should show unknown error") {
                assertEquals(liveData!!.observedValues[1]!!.state, unknownErrorResource.state)
            }
        }
    }
})