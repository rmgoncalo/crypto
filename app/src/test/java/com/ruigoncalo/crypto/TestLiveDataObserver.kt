package com.ruigoncalo.crypto

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer

class TestLiveDataObserver<T> : Observer<T> {

    val observedValues = mutableListOf<T?>()

    override fun onChanged(value: T?) {
        observedValues.add(value)
    }
}

fun <T> LiveData<T>.testObserver() = TestLiveDataObserver<T>().also {
    observeForever(it)
}