package com.ruigoncalo.crypto

import android.arch.core.executor.ArchTaskExecutor
import android.arch.core.executor.TaskExecutor
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.functions.Function
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.jetbrains.spek.api.dsl.Pending
import org.jetbrains.spek.api.dsl.SpecBody
import java.util.concurrent.Callable

inline fun SpecBody.rxGroup(description: String, pending: Pending = Pending.No,
                            crossinline body: SpecBody.() -> Unit) {
    group(description, pending) {

        val schedulerInstance = Schedulers.trampoline()

        val schedulerFunction: io.reactivex.functions.Function<Scheduler, Scheduler> = Function { schedulerInstance }

        val schedulerFunctionLazy: io.reactivex.functions.Function<Callable<Scheduler>, Scheduler> = Function { schedulerInstance }

        beforeEachTest {
            RxAndroidPlugins.reset()
            RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerFunctionLazy)

            RxJavaPlugins.reset()
            RxJavaPlugins.setIoSchedulerHandler(schedulerFunction)
            RxJavaPlugins.setNewThreadSchedulerHandler(schedulerFunction)
            RxJavaPlugins.setComputationSchedulerHandler(schedulerFunction)

            ArchTaskExecutor.getInstance().setDelegate(object : TaskExecutor() {
                override fun executeOnDiskIO(runnable: Runnable) {
                    runnable.run()
                }

                override fun postToMainThread(runnable: Runnable) {
                    runnable.run()
                }

                override fun isMainThread(): Boolean {
                    return true
                }
            })
        }

        body()

        afterEachTest {
            RxAndroidPlugins.reset()
            RxJavaPlugins.reset()
            ArchTaskExecutor.getInstance().setDelegate(null)
        }
    }
}