package com.ruigoncalo.data.store

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.reset
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.ruigoncalo.data.cache.Cache
import com.ruigoncalo.domain.model.BitcoinRates
import io.reactivex.observers.TestObserver
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import polanski.option.Option

class ReactiveStoreSpec : Spek({

    val cache: Cache<String, BitcoinRates> = mock()
    val tested = ReactiveStore(cache)
    var testObserver = TestObserver<Option<BitcoinRates>>()

    val mockKey = "key"
    val mockValue = BitcoinRates("desc", "USD", listOf())

    beforeEachTest {
        testObserver = TestObserver()
    }

    afterEachTest {
        reset(cache)
    }

    describe("get singular item from store") {

        context("item exists in cache") {
            beforeEachTest {
                whenever(cache.get(mockKey)).thenReturn(mockValue)
                tested.getSingular(mockKey).subscribe(testObserver)
            }

            it("should return item") {
                testObserver.assertValue(Option.ofObj(mockValue))
            }

            it("should not complete") {
                testObserver.assertNotComplete()
            }
        }

        context("item does not exist in cache") {
            beforeEachTest {
                whenever(cache.get(mockKey)).thenThrow(IllegalStateException())
                tested.getSingular(mockKey).subscribe(testObserver)
            }

            it("should return none") {
                testObserver.assertValue(Option.none())
            }

            it("should not complete") {
                testObserver.assertNotComplete()
            }

            it("should not return errors") {
                testObserver.assertNoErrors()
            }
        }
    }

    describe("put singular item in store") {
        beforeEachTest {
            tested.putSingular(mockKey, mockValue).subscribe(testObserver)
        }

        it("should store the item") {
            verify(cache).put(mockKey, mockValue)
        }

        it("should complete") {
            testObserver.assertComplete()
        }
    }

})