package com.ruigoncalo.data

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.reset
import com.nhaarman.mockito_kotlin.whenever
import com.ruigoncalo.data.model.BitcoinRateValueRaw
import com.ruigoncalo.data.model.BitcoinRatesRaw
import com.ruigoncalo.domain.model.BitcoinRateValue
import com.ruigoncalo.domain.model.BitcoinRates
import com.ruigoncalo.domain.utils.DateProvider
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import java.util.*
import kotlin.test.assertEquals

class BitcoinRatesMapperSpec : Spek({

    val dateProvider: DateProvider = mock()
    val tested = BitcoinRatesMapper(dateProvider)

    val timestamp = 123L
    val date = Date()
    val unit = "USD"

    beforeEachTest {
        whenever(dateProvider.convertToDate(timestamp)).thenReturn(date)
    }

    afterEachTest {
        reset(dateProvider)
    }

    given("bitcoin rates mapper") {
        val mockBitcoinRatesRaw = BitcoinRatesRaw(
                "name", unit, "period", "desc",
                listOf(BitcoinRateValueRaw(timestamp, 1.0)))

        val mockBitcoinRates = BitcoinRates("desc", unit,
                listOf(BitcoinRateValue(date, 1.0)))

        it("should map raw to model") {
            assertEquals(tested.map(mockBitcoinRatesRaw), mockBitcoinRates)
        }
    }
})