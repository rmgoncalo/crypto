package com.ruigoncalo.data

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.reset
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.ruigoncalo.data.model.BitcoinRatesRaw
import com.ruigoncalo.data.remote.BlockchainApi
import com.ruigoncalo.data.store.Store
import com.ruigoncalo.domain.model.BitcoinRates
import com.ruigoncalo.domain.model.Timespan
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import polanski.option.Option
import java.util.concurrent.TimeUnit


class BitcoinRatesRepositorySpec : Spek({

    val api: BlockchainApi = mock()
    val store: Store<String, BitcoinRates> = mock()
    val mapper: BitcoinRatesMapper = mock()
    val tested = BitcoinRatesRepository(api, store, mapper)
    var testObserver = TestObserver<Option<BitcoinRates>>()

    val mockBitcoinRatesRaw = BitcoinRatesRaw("name", "USD", "period", "desc", listOf())
    val mockBitcoinRates = BitcoinRates("desc", "USD", listOf())
    val mockTimespan = Timespan.DAYS180

    beforeEachTest {
        testObserver = TestObserver()

        whenever(mapper.map(mockBitcoinRatesRaw)).thenReturn(mockBitcoinRates)
        whenever(api.getBitcoinRates(mockTimespan.value)).thenReturn(Single.just(mockBitcoinRatesRaw))
        whenever(store.getSingular(mockTimespan.value)).thenReturn(Observable.just(Option.ofObj(mockBitcoinRates)))
        whenever(store.putSingular(mockTimespan.value, mockBitcoinRates)).thenReturn(Completable.complete())
    }

    afterEachTest {
        reset(api)
        reset(store)
        reset(mapper)
    }

    given("bitcoin rates repository") {

        describe("get bitcoin rates") {

            context("store returns rates") {
                beforeEachTest {
                    tested.getBitcoinRates(mockTimespan).subscribe(testObserver)
                }

                it("should return rates") {
                    with(testObserver) {
                        assertValue(Option.ofObj(mockBitcoinRates))
                        assertNoErrors()
                        assertComplete()
                    }
                }
            }

            context("store returns error") {
                val error = Throwable()
                beforeEachTest {
                    whenever(store.getSingular(mockTimespan.value)).thenReturn(Observable.error(error))
                    tested.getBitcoinRates(mockTimespan).subscribe(testObserver)
                }

                it("should return error") {
                    testObserver.assertError(error)
                }
            }
        }

        describe("fetch bitcoin rates") {

            context("api returns raw rates") {
                beforeEachTest {
                    tested.fetchBitcoinRates(mockTimespan).subscribe(testObserver)
                }

                it("should store mapped rates") {
                    verify(store).putSingular(mockTimespan.value, mockBitcoinRates)
                }
            }

            context("api return error") {
                val error = Throwable()
                beforeEachTest {
                    whenever(api.getBitcoinRates(mockTimespan.value)).thenReturn(Single.error(error))
                    tested.fetchBitcoinRates(mockTimespan).subscribe(testObserver)
                }

                it("should not store") {
                    verify(store, never()).putSingular(mockTimespan.value, mockBitcoinRates)
                }

                // without awaitDone sometimes does not return error
                it("should return error"){
                    testObserver.awaitDone(1, TimeUnit.SECONDS).assertError(error)
                }
            }
        }

    }

})