package com.ruigoncalo.data

import com.ruigoncalo.data.model.BitcoinRatesRaw
import com.ruigoncalo.domain.Mapper
import com.ruigoncalo.domain.model.BitcoinRateValue
import com.ruigoncalo.domain.model.BitcoinRates
import com.ruigoncalo.domain.utils.DateProvider
import javax.inject.Inject

class BitcoinRatesMapper @Inject constructor(
        private val dateProvider: DateProvider) : Mapper<BitcoinRatesRaw, BitcoinRates> {

    override fun map(r: BitcoinRatesRaw): BitcoinRates {
        return BitcoinRates(
                unit = r.unit,
                description = r.description,
                values = r.values.map { rawValue ->
                    BitcoinRateValue(dateProvider.convertToDate(rawValue.timestamp), rawValue.value)
                })
    }
}