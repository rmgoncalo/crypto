package com.ruigoncalo.data.store

import com.ruigoncalo.data.cache.Cache
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import polanski.option.Option

/**
 * ReactiveStore gets and puts data on the cache
 *
 * If the value is not cached when getSingular, an Option.none() is returned
 *
 * When a value is stored in the cache, it is emitted to getSingular() subscribers
 */
class ReactiveStore<Value>(private val cache: Cache<String, Value>) : Store<String, Value> {

    private val subject: PublishSubject<Option<Value>> = PublishSubject.create()

    override fun getSingular(key: String): Observable<Option<Value>> {
        return Observable.defer { Observable.just(Option.ofObj(cache.get(key))) }
                .onErrorResumeNext { _: Throwable -> Observable.just<Option<Value>>(Option.none()) }
                .concatWith(subject)
    }

    override fun putSingular(key: String, value: Value): Completable {
        return Completable.fromCallable {
            cache.put(key, value)
            subject.onNext(Option.ofObj(value))
        }
    }
}