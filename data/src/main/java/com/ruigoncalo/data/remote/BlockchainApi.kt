package com.ruigoncalo.data.remote

import com.ruigoncalo.data.model.BitcoinRatesRaw
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Source: www.blockchain.com
 *
 * query params "timespan" defines the time frame in which the rates should hold
 * ex: timespan=60days
 */
interface BlockchainApi {

    @GET("charts/market-price")
    fun getBitcoinRates(@Query("timespan") timespan: String): Single<BitcoinRatesRaw>
}