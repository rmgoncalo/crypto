package com.ruigoncalo.data.model

import com.google.gson.annotations.SerializedName

data class BitcoinRatesRaw(@SerializedName("name") val name: String,
                           @SerializedName("unit") val unit: String,
                           @SerializedName("period") val period: String,
                           @SerializedName("description") val description: String,
                           @SerializedName("values") val values: List<BitcoinRateValueRaw>)