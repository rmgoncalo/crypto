package com.ruigoncalo.data.model

import com.google.gson.annotations.SerializedName

data class BitcoinRateValueRaw(@SerializedName("x") val timestamp: Long,
                               @SerializedName("y") val value: Double)