package com.ruigoncalo.data

import com.ruigoncalo.data.remote.BlockchainApi
import com.ruigoncalo.data.store.Store
import com.ruigoncalo.domain.Repository
import com.ruigoncalo.domain.model.BitcoinRates
import com.ruigoncalo.domain.model.Timespan
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import polanski.option.Option

/**
 * getBitcoinRates according to the timespan should get from the store
 *
 * fetchBitcoinRates according to the timespan should get from the api and store the result
 *
 */
class BitcoinRatesRepository (private val api: BlockchainApi,
                                                 private val store: Store<String, BitcoinRates>,
                                                 private val mapper: BitcoinRatesMapper) : Repository<Timespan, BitcoinRates> {

    override fun getBitcoinRates(params: Timespan): Observable<Option<BitcoinRates>> {
        return store.getSingular(params.value)
    }

    override fun fetchBitcoinRates(params: Timespan): Completable {
        return api.getBitcoinRates(params.value)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .map(mapper::map)
                .flatMapCompletable { rate -> store.putSingular(params.value, rate) }
    }
}